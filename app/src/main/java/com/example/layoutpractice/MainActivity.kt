package com.example.layoutpractice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        linearLayoutBtn.setOnClickListener {
           val linearLayoutIntent = Intent(this, LinearLayoutActivity::class.java)
            startActivity(linearLayoutIntent)
        }

        relativeLayoutBtn.setOnClickListener {
            val relativeLayoutIntent = Intent(this, RelativeLayoutActivity::class.java)
            startActivity(relativeLayoutIntent)
        }

        tableLayoutBtn.setOnClickListener {
            val tableLayoutIntent = Intent(this, TableLayoutActivity::class.java)
            startActivity(tableLayoutIntent)
        }
    }
}